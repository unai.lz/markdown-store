# Instrucciones

El metodo de expresión regular que se usa en estos documentos segmenta los botones según las partes que sean necesarias.

Se colocan entre <@ @> y aquí dentro se ennumeran según su nombre, categoría o número.

Por ejemplo "<@b_p_1.1 Proceso guiado.        b_t_1.1@>" sería un ejemplo de nombre de botón funcional, donde "b" representa "boton", "p" representa proceso y "1.1" significaría que es el primer botón de el primer grupo de botones.


También mencionar que al final, entre el nombre y su cierre hay que añadir la cantidad de caracteres que tenga su cierre. Si "b_t_1.1@>" tiene 9 caracteres, despues de "proceso guiado" hay que poner 9 espacios.

## Botones

<@b_1 Ejemplo Botón.    b_1@>
<@b_2 Ejemplo de grupo.    b_2@>