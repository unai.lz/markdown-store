<!-- Prueba. -->


# Manual de Microhondas. 


# Especificaciones.

- Modelo: MWE225 G.
- Alimentación eléctrica: 230 V ~ 50 Hz
- Entradade potencia nominal (microhondas): 1050 W
- Salida de potencia nominal (microhondas): 700 W
- Entrada de potencia nominal (grill): 1000 W
- Capacidad de hrono: 20 litros
- Diámetro del plato giratirio: 255 mm
- Dimenciones externas (largo x ancho x alto): 439.5mm x 340mm x 258.2mm
- Peso neto: 10.95kg aprox.

# Seguridad

>Precauciones para evitar la posible exposición a una excesiva energia de Microhondas:

1. No intente hacer funcionar este horno con la puerta abierta, ya que puede producirse una exposición peligrosa a las microondas. Es importante no forzar o manipular los cierres de seguridad
2. No coloque ningún objeto entre el frente del horno y la puerta ni deje que se acumulen suciedad o restos de limpiador en las superficies de sellado
3. ADVERTENCIA: Si la puuerta o sus juntas estan dañadas, el horno no debe utilizarse hasta que haya sido reparado por una persona cualificada.

IMPORTANTE: Si el aparato no se limpia adeccuadamente, su superficie puede degradarse acortando el tiempo de vida útil del mismo y puede crearse una situación de peligrosidad.

>## Instrucciones importantes de seguridad

>### Advertencia: 
>Para reducir el riesgo de incendio, descargar eléctricas o lesiones a las personas, exposición excesiva a las microondas cuando se utilice el aparato, siga las siguientes precauciones básicas.

>1. ! Los líquidos u otros alimentos no deben ser calentados jamás en recipientes sellados ya que podrían estallar.
>2. ! Es peligroso, para quien no sea un técnico autorizado, quitar la cobertura de protección de la exposición a la energía de microondas.
>3. ! Este aparato puede ser usado por niños mayores de 8 años, por personas con limitadas capacidades físicas, mentales o sensoriales y personas sin experiencia o conocimiento, y siempre bajo la supervisión de un adulto y tras haber sido intruidos sobre el uso seguro y correcto del aparato y los peligros que entraña su uso. Los niños no deben jugar con el aparato. La limpieza y mantenimiento del aparato no deben realizarla niño a menos que sean mayores de 8 años y siempre bajo la supervisión de un adulto.
4. Mantener el aparto (y su cable) fuera del alcance de niños menores de 8 años.
5. Utilice solamente utensilios adecuados para ser usados con hornos microondas.
6. El horno debe limpiarse con regularidad y se debe quitar cualquier tipo de resto alimenticio.
7. Lea y respete las precauciones para evitar posibles exposiciones excesivas a la energía de microondas.
8. Cuando caliente alimentos en recipientes de plástico o de papel vigile el horno ya que se podría incendiar.
9. Si ve que sale humo, apague y desenchufe el horno de la toma de corriente y mantenga la puerta ccerrada para sofocar las posibles llamas.
10. No cueza excesivamente los alimentos.
11. No utilice el interior del horno como almacenaje, No deje productos como pan, bizcochos, etc en el interior del horno.
12. Quite los cierres metálicos y las asas de metal de los recipientes de papel/bolsas de papel o plástico antes de meterlos en el horno.
13. Instale o coloque e horno siguiendo las instrucciones de instalación suministradas
14. Los huevos con cascara y los huevos duros no deben calentarse en el microondas, podrían estallar incluso despues de haber terminado el calentamiento en el microhondas.
15. Este aparato está diseñado sólo para calentar, no para usos industriales o de laboratorio. Sólo apto para utilizar en hogares o lugares similares como:
>1. Areas de cocina para el personal de tiendas, oficinas y otros entornos laborales.
>2. Clientes en hoteles, casas rurales y otros tipos de entornos residenciales
>3. Granjas
>4. Entornos residenciales como aquellos que ofrecen alojamiento y desayuno.
16. Si el cable original del aparato se ha dañado, debe ser sustituido por el fabricante, por el servicio de mantenimiento o por el personal cualificado para prevenir cualquier situación de peligro.
17. No guarde y no utilice éste electrodomestico al aire libre.
18. No utilice este horno cerca del agua, del fregadero mojado o cerca de una piscina.
19. La temperatura de las superficies accesibles podría ser elevada durante el funcionamiento del electrodoméstico. Mantenga el cable de alimentación lejos de superficies caliente y no cubra el horno por ningúna razón.
20. No deje que el cable de alimentación quede colgando sobre el borde de la mesa o de la superficie de trabajo.
21. Si no se siguen las indicaciones de limpieza, se podrían dañar las supperficies y ello podría acortar la vida de electrodoméstico drásticamente, además de provocar situaciones peligrosas.
22. Mezcle o agite siempre el contenido de biberones y recipientes para alimentos de los bebés y controle la temperatura de los mismos para evitar quemaduras.
23. El calentamiento de bebidas en el microondas podría provocar un proceso de ebullición retardada, por lo tanto tenga cuidado cuando mueva el recipiente.
24. Este aparato no es apto para personas o niños con capacidades físicas, sensoriales o mentales limitadas o con falta de experiencia y conocimientos, a menos que estén bajo supervisión de un adulto o hayan sido previamente instruidos sobre el uso seguro y correcto del aparato y los peligros que entraña su uso.
25. Los niños no deben jugar con el aparato y deben estar siempre bajo la supervisión de un adulto.
26. Este aparato no se puede operar con un temporizador externo o por control remoto.
>27. ! El aparato y sus partes accesibles se calientan durante el uso. Mantener lejos del alcance de los niños.
28. No utilizar limpiadores a vapor.
29. El aparato se calienta durante el uso. Evite tocar elementos calientes dentro del aparato.
30. Utilice solamente una sonda de temperatura recomendada por el fabricante (solo para hornos con sensor de temperatura).
>31. ! El aparato y sus partes accesibles se calientan durante el uso. Evite tocar sus elements calientes. Mantener lejos del alcance de niños menores de 8 años a menos que estén bajo la supervisión de un adulto.
32. El horno microondas debe operarse con la puerta decorativa abierta (para hornos con puerta decorativa).
33. La parte trasera del aparato debe colocarse contra una pared.
34. El horno microondas no debe encastrarse en un mueble a menos que haya sido previamente testeado para éste tipo de instalación
35. El horno microondas está destinado a calentar alimentos y bebidas. Existe el riesgo de ocasionar lesiones, igniciones, o fuegos al secar alimentos o ropa, o calentar almohadillas térmicas, zapatillas, esponjas, paños húmedos y objetos similares.


>## Reducir el riesgo de daños a las personas

>### Puesta a tierra
Peligro de descargas eléctricas. El contacto con ciertos componentes internos puede producir lesiones graves o incluso la muerte.
No desmonte el electrodomestico.
> ## ! Advertencia: 
>Peligro de descargas eléctricas. El uso inadecuado de puesta a tierra puede producir descargas eléctricas. No enchufe el aparato hasta que esté correctamente instalado y puesto a tierra.
Este electrodoméstico debe instalarse con toma de tierra. En el caso de que se produzca un cortocircuito, la toma de tierra reduce el riesgo de descarga eléctrica por la existencia de un cable por el que puede escaparse la corriente eléctrica. Este electrodoméstico está equipado con un cableque tiene un conductor de tierra y un enchufecon toma de tierra.
Consulte a un electricista cualificado o a un técnic de mantenimiento si no comprende totalmente las instrucciones sobre la puesta a tierra o si tiene algúna duda sobre la forma en que el electrodomestico queda correctamente conectado a tierra. Si fuera necesario un cable alargador, use solamente un alargados de tres hilos.

# Procesos guiados

>## Limpieza:
Cerciorese de que el horno esté desconectado de la red.
1. Limpie el interior del horno con un paño ligeramente húmedo tras utilizarlo.
2. Limpie los accesorio como hace habitualmente en agua con detergente.
3. El marco de la puerta, la junta y la piezas cercanas han de limpiarse cuidadosamente con un paño húmedo cuando estén sucias.
4. No utilice limpiadores abrasivos o ásperos ni raspadores metálicos para limpiar el cristal de la puerta de horno, ya que podrían rayar la superficie, que se podría romper.
5. Truco de limpieza: Para una mejor limpieza de la cavidad para eliminar restos de comida de las paredes del horno: Coloque medio limón en un bol, añada 300ml (media pinta) de agua y ponga el microondas a potencia máxima durante 10 minutos. Limpie el interior del horno con un trapo seco y suave.

>## Prueba de utensilios:
> ### ! Advertencía: 
>Es peligroso para cualquiera que no esté cualificado realizar reparaciones o mantenimiento que supongan retirar la tapa de mica que protege de la exposición a las microondas
1. Llene un recipiente apto para el horno microondas con un vaso de agua fría (250ml) e introdúzcalo en el horno microondas con el utensilio en cuestión.
2. Cocine a la potencia más alta durante 1 minuto.
3. Toque con cuidado el utensilio. Si está caliente, no lo utilice para cocinar en el horno microondas.
4. No supere 1 minuto de cocción.


# Partes a describir

>## Cuadro de mandos

 <@b_t_1_1  Tecla de cocinado.        @b_t_1_1>
 <@b_t_1_2  Descongelacion por peso/tiempo.        @b_t_1_2>
 <@b_t_1_3  Relog/temporizador.        @b_t_1_3>
 <@b_t_1_4  Cancelar/parar.        @b_t_1_4>
 <@b_t_1_5  Inicio/30 segundos.        @b_t_1_5>
 <@b_t_1_6  Mando selector.        @b_t_1_6>

# Miscelanea

## Materiales que pueden usarse en el microhondas:

| Utensilios | Observaciones |
| ---------- | ------------- |
| Papel de aluminio | 
| Plato tostador  
| Menaje | 
| Frascos de vidrio | 
| Vajilla de vidrio | 
| Bolsas de cocinar |  
| Platos y vasos de papel |  
| Plástico | 
| Plástico trasparente | 
| Termómetros | 
| Papel vegetal | 

## Materiales que hay que evitar e el horno microhondas

| Utensilios | Observaciones |
| ---------- | ------------- |
| Bandejas e aluminio |  |
| Envase de cartón para alimentos con mango de metal |  |
| Utensilios metálicos o con adornos metálicos |  |
| Tiras de atar metálicas |  |
| Bolsas de papel |  |
| Poliespán |  |
| Madera |  |



# Ajuste del horno microhondas

<!-- Aquí me planteo aplicar directamente éste caso práctico desde el Markdown a la escena y crear un pequeño workflow para aplicar en un mismo modulo (por así decirlo) los dos tipos de visualización en AR para poder enseñar completamente el proceso -->

## Instalación

0. Escoja una supreficie nivelada que tenga espacio suficiente para los venteos de entrada y salida.

1. La altura mínima de instalación es de 85cm.

2. La parte trasera del aparato debe colocarse contra una pared.
>Deje un espacio mínimo de 30 cm por encima del horno. Se requiere un espacio mínimo de 20cm entre el horno y cualquier pared adyacente.

3. No quite las patas de la parte inferior del horno.

4. El bloqueo de las aberturas en la entrada y en la salida podría dañar el horno.

5. Coloque el horno lo más lejos posible de radios o televisores. El funcionamiento del horno microondas puede provocar interferencias en la recepción de estos aparatos.

6. Conecte el horno a una toma doméstica estandar.
>Controle que el voltaje y la frecuencia sean iguales al voltaje y a la frecuencia indicados en la placa de los valores nominales.

> ### ! Advertencia
>No instale el horno sobre una cocina u otros eléctrodomesticos que generen calor. Si se instala cerca o sobre una fuente de calor el horno podría dañarse y la garantía no tendría validez.


### Nombres de piezas y accesorios del horno

1. Panel de control
2. Eje giratorio
3. Conjunto del aro giratorio
4. Bandeja de vidrio
5. Ventana de observación
6. Conjunto de la puerta
7. Sistema de cierre de seguridad


### Instalación del plato giratorio

1. ! No ponga nunca la bandeja de vidrio boca abajo. La bandeja de vidrio debe poder moverse siempre.

2. Para la cocción deben usarse siempre la bandeja de vidrio y el conjunto del aro giratorio.

3. Todos los alimentos y sus recipientes deben ponerse siempre sobre la bandeja de vidrio para la cocción.

4. Si la bandeja de vidrio o el conjunto del aro giratorio se rompen o agrietan, póngase en contacto con el centro de servicio autorizado más cercano.

### ! Instalación de la encimera 

Retire todo el material de embalaje y los accesorios. 
Revise el horno en busca de daños, como puerta con abolladuras o rota. No intale el horno si está dañado.
> ! No retire la tapa de color marrón claro de mica fijada en la cavidad del horno para proteger el magnetrón.


### Ajuste del relog

<@m_t_1.0  Al conectar el horno a la red de alimentación en la pantalla se mostrará "0:00" y oirá un pitido.         @m_t_1.0>
<@m_t_1.1  Pulse "Relog y temporizador", la cifra de las horas comenzará a parpadear.         @m_t_1.1>
<@m_t_1.2 Gire el mando para ajustar la cifra de las horas, la hora introducida debe estar cimpendida entre 0 y 23.         @m_t_1.2>
<@m_t_1.3 Pulse el boton "Relog temporizador", la cifra de los minutos empezará a parpadear.         @m_t_1.3>
<@m_t_1.4 Gire el mando para ajustar la cifra de los minutos. La cifra introducida debe estar comprendida entre 0 y 59.         @m_t_1.4>
<@m_t_1.5 Pulse el boton "Relog temporizador" para finalizar el ajuste del relog. El indicador : comenzará a parpadear.         @m_t_1.5>

### Usar temporizador

<@m_t_2.1 Pulse "Relog temporizador" dos veces, la pantalla mostrará 0:00.         @m_t_2.1>
<@m_t_2.2 Gire el mando para seleccionar el tiempo. El máximo son 95 minutos.         @m_t_2.2>
<@m_t_2.3 Pulse el boton "+30 sec" para confirmar.         @m_t_2.3>
<@m_t_2.4 Cuando se alcanza el tiempo programado, el indicador del temporizador se apagará.         @m_t_2.4>

### Función grill

<@m_t_3.1 Pulse el boton "Mode", en la pantalla se mostraá "P100".         @m_t_3.1>
<@m_t_3.2 Pulse el boton "Mode" varias veces o gire el mando para seleccionar la función al grill.         @m_t_3.2>
<@m_t_3.3 Cuando en la pantalla aparezca "G" pulse el boton "+30 sec" para confirmar la selección.         @m_t_3.3>
<@m_t_3.4 Gire el mando para ajustar el tiempo de cocción. El tiempo de cocción debe estar comprendido entre 0:05 y 95:00.         @m_t_3.4>
<@m_t_3.5 Pulse el boton "+30 sec" para comenzar a cocinar.         @m_t_3.5>




# Expresiones

<@b_g_1 Usar el Microondas MWE225G.     @b_g_1>
<@b_l_1 Manual.     @b_l_1>
<@b_l_2 Pagina Web.     @b_l_2>
<@b_o_1 Partes.     @b_o_1>
<@b_o_2 Ajustar relog.     @b_o_2>
<@b_o_3 Usar temporizador.     @b_o_3>
<@b_o_4 Funcion grill.     @b_o_4>